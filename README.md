## Requirement
**Backend**
- visual c++ redistributable packages (khusus untuk OS Windows)
- XAMPP (Min. PHP 7.3)
- Composer

## 1. How To Clone
```bash
git clone https://gitlab.com/ahmad.qorthony/waste4changetest_backend.git
```


## 2. Installation
- Install melalui composer
```bash
composer install
```
- Copy .env.example menjadi .env
```bash
cp .env.example .env
```

- Generate key
```bash
php artisan key:generate
```

- Setting Database
    - Buat Database MySQL baru, kemudian biarkan kosong
    - buka file .env
    - edit DB_DATABASE sesuai nama Database yang dibuat tadi
    - edit DB_USERNAME sesuai pengaturan, jika belum diatur isi dengan root
    - edit DB_PASSWORD sesuai pengaturan, jika belum diatur kosongi saja

- Migrate Database
```bash
php artisan migrate
```

- Seed Database 
```bash
php artisan db:seed
```


## 3. Run
```bash
php artisan serve
```
## Next step
setup <a href="https://gitlab.com/ahmad.qorthony/waste4changetest_frontend" >Frontend Project</a> 
