<?php

namespace App\Http\Controllers;

use App\Helper\Id;
use App\Models\Type;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::with('material')->get();
        return response()->json([
            'types'=>$types
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            // 'id'=>'required',
            'material_id'=>'required|exists:materials,id',
            'name'=>'required'
        ]);

        // dd(Id::generate('types',$request->name));
        $validated['id']=Id::generate('types',$request->name);

        try {
            $type = Type::create($validated);

            return response()->json([
                'message'=>'type created',
                'type'=>$type
            ], Response::HTTP_CREATED);

        } catch (QueryException $e) {

            return response()->json([
                'message'=>"Failed ".$e->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = Type::findOrFail($id);

        try {
            $type->delete();
            return response()->json(['message'=>'type deleted'], Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message'=>"Failed ".$e->errorInfo
            ]);
        }
    }
}
