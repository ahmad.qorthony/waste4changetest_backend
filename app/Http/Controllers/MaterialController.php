<?php

namespace App\Http\Controllers;

use App\Helper\Id;
use App\Models\Material;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = Material::all();
        return response()->json([
            'materials'=>$materials
        ], Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            // 'id'=>'required',
            'name'=>'required'
        ]);

        $validated['id'] = Id::generate('materials', $request->name);

        try {
            $material = Material::create($validated);
            return response()->json([
                'message'=>'material created',
                'material'=>$material
            ], Response::HTTP_CREATED);
        } catch (QueryException $e) {
            return response()->json([
                'message'=>"Failed ".$e->errorInfo
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $material = Material::findOrFail($id);

        try {
            $material->delete();
            return response()->json(['message'=>'material deleted'], Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message'=>"Failed ".$e->errorInfo
            ]);
        }
    }
}
