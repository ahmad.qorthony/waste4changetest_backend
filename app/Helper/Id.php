<?php

namespace App\Helper;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Id{
    const MATERIAL_PREFIX = 'MAT';
    const TYPE_PREFIX = 'TYP';

    static public function generate($target, $name)
    {
        $prefix = $target == 'materials' ? self::MATERIAL_PREFIX : self::TYPE_PREFIX;
        $infix = self::getIncrement($target);
        $suffix = self::suffix($name);

        return $prefix."-".$infix."-".$suffix;
    }

    static private function getIncrement($target)
    {
        $lastId = DB::table($target)->latest()->get()->pluck('id');

        if (count($lastId) != 0) {
            list( ,$increment, )  = explode("-",$lastId[0]);
        } else {
            $increment = 0;
        }

        $increment = (int) $increment + 1;

        $infix = '';

        if ($increment < 9) {
            $infix = '00'. $increment;
        } elseif ($increment < 99) {
            $infix = '0'.(string) $increment;
        } else {
            $infix = (string) $increment;
        }

        return $infix;
    }

    static private function suffix($name)
    {
        $result='';
        $name = preg_replace( '/[^a-z0-9 ]/i', '', $name);
        $words = explode(" ", $name);
        if (count($words)==1) {
            $result = substr($name, 0, 3);
        } elseif (count($words)==2) {
            $result = substr($words[0],0,1).substr($words[1],0,1);
        } else {
            $result = substr($words[1], 0, 3);
        }
        return Str::upper($result);
    }
}
