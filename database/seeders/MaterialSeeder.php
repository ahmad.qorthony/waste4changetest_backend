<?php

namespace Database\Seeders;

use App\Models\Material;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('materials')->insert(
            [
                [
                    "id"=>"MAT-001-RES",
                    "name"=>"Residu"
                ],
                [
                    "id"=>"MAT-002-PLS",
                    "name"=>"Plastik"
                ],
                [
                    "id"=>"MAT-003-UBC",
                    "name"=>"UBC"
                ],
                [
                    "id"=> "MAT-004-LOG",
                    "name"=> "Logam"
                ]
            ]
        );
    }
}
